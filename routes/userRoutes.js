const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const userController = require("../controllers/userController")
const user = require("../models/user.js")

/* 
create a funciton with the "/checkEmail" endpoint that will be able to use the checkEmail function in the userController

Reminder that we have a parameter needed in the checkEmail funciton and that is data from request body

send a screenshot
*/
 
/* 
router.get("/checkEmail", (req, res) => {
    userController.checkEmail(req.body).then(result => res.send(result));
}) 
*/

router.post("/checkEmail", (req, res) => {
    userController.checkEmail(req.body).then(resultFromController => res.send(resultFromController));
})

// user registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// user login
router.post("/login", (req, res) => {
    userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})

//auth.verify - ensures that a user is logged in before procedding to the next part of the code

router.get("/details", auth.verify, (req,res) => {
    //decode - decrypts the token insdie the authorizzation (which is in the headers of the request)
    // req.headers,authorization contains the token that was created for the users
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})

/* 
    mini actvity
        create a post request under the "/enroll" endpiont
        check if the user is logged in
        create a variable and store an object with userId and courseId as frilds
            userId - decoded token of the user
            courseId - the course id present in the request id
        user the variable as a parameter of the enroll function 

*/

router.post("/enroll", auth.verify, (req,res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        courseId: req.body.courseId 
    }
    userController.enroll(data).then(result => res.send(result))
})
    
module.exports = router;
