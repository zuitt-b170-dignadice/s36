const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController")
const { route } = require("./userRoutes.js")

/* 
    Activity
    Create a reoute that will let an admin perform addCourse function in the courseController

        verify that the user is logged in
        decode the token for that usser
        use the id, isAdmin, and request body to perform the function in the courseController

            the id and isAdmin are parrts of an object
*/

/* 

s34 business logic (courseRoutes)
ACTIVITY
create a route that will let an admin perform addCourse function in the courseController
    verify that the user is logged in
    decode the token for that user
    use the id, isAdmin, and request body to perform the function in courseController
        the id and isAdmin are parts of an object
Business Logic (courseController)
ACTIVITY s34
    1. find the user in the database
        find out if the user is admin

    2. if the user is not an admin, send the response "You are not an admin"

    3. if the user is an admin, create the new Course object
            -save the object
                -if there are errors, return false
                -if there are no errors, return "Course created sucessfully"

*/

router.post("/", auth.verify, (req,res) => {
    const userData = auth.decode(req.headers.authorization)

    courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController))
})

/* 

e commerce websites

*/

/* 
create a route that will retrieve  all of our products/courses
    will require login/register functions
*/



router.get("/",(req, res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

// retrieve all active courses
router.get("/active", (req, res) => {
    courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})



/* 
create a route that will retrieve a coursee that will retrieve a course
*/

router.get("/:courseId",  (req, res) => {
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(result => res.send(result))
})

/* 
    in getting all of the documents, in case we need multiple of them, place the route with the criteria to the find method.
*/

// update a coursoe

/* 
 Delete is never a norm in databases 
 use "/courseId/archiveCourse" and set a PUT resquest to archive a course by changing the active status
*/

router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})

/* 
Archive a course

routes business logic:

 use /archiveCourse and send a PUT request to archive a course by changing the active status
controllers business logic: 

    1. create a updateCourse object with the content from the reqBody
            reqBody should have the isActive status of the course to be set to false
    2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
            handle the errors that may arise
                error/s - false
                no errors - true
*/

router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body).then(result => res.send(result))
})



router.put("/:courseId/archiveCourse", auth.verify, (req,res) => {
    courseController.archiveCourse(req.params, req.body)
    .then(result => res.send(result))
})


 









module.exports = router;