const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// access to routes

const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

// server
const app = express();
const port = 4000

app.use(cors()) // allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded({extended:true}))


app.use("/api/users", userRoutes)
app.use("/api/courses", courseRoutes)

mongoose.connect("mongodb+srv://admin:admin@cluster0.h8y6g.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("we're connected to the database"))

//app.listen(port, () => console.log(`API now online at port ${port}`))


app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`))

//process.env.PORT works if you areo deploying in the api in a host like Heroku, this code allows the app to use the environment of that host (Heroku) in running the server

/* 

    1 register in heroku
    2 check if you have heroku in your device
    3 create a procfile file
        i it should have P 
        - it should not have any file extendion
        - inserting the text "web : node app"
    4 login your heroku thorugh the git bash terminal
        - "heroku login"
            press any key
            login us ing yor heroku account
    5 heroku create/ deploy 
    6. git push heroku master to push the updates in heroku
*/

// activty s34    

/* Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.
 */

/* 

Capstone 2 

Data model design and user reigstration

User registration
Usert Auth
Set user an admin (admin)
retrieve all active producsts
retrieve single product
create product (admin only)
update product information (admin)
archive product (admin )
Non-admin user chackout (create order)
retrieve authenticated user's orders
retrieve all orsers

User
Name string
email string
password string
is admin bool

product
name (strng)
descriptioon (string)
price (number)
isActive (date)
created on (date)

order 
totalAmount number
purchasedOn (date)
must be assoc with (user who owns the order
    products that belong to the order)




*/