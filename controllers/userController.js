// set up dependencies

const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") // used to encrypt user passwords
const { off } = require("../models/user.js")

// check if the email exists
/* 
    1. check foe the email in the database
    2. send the result as a response (with error handling)
*/

/* 
 it is conventional for the devs to use Boolean in sending return responses especially with the backend application
*/

module.exports.checkEmail = (requestBody) => {
    return User.find({email: requestBody.email}).then((result, error) => {
        if(error){
            console.log(error)
            return false
        } else {
            if (result.length > 0) {
                // return result
                return true
            } else {
                // return res.send("email does not exist")
                return false
            }
        }
    })
}

//  USER Registration

/* 
    1. create a new user with the information from the request body
    2. Make sure that the password is encrypted.
    3. Save the new user
*/

module.exports.registerUser = (reqBody) => {
    let newUser = new User(
        {
            firstName : reqBody.firstName,
            lastName : reqBody.lastName,
            age : reqBody.age,
            gender: reqBody.gender,
            email: reqBody.email,
            // hashSync is a funmction of bcrypt the encrypts the password
                // 10 is the number of rounds/times it runs the algorithm to the reqBody.password
                // max 72 implementations
            password : bcrypt.hashSync(reqBody.password, 10),
            mobileNo: reqBody.mobileNo
        }
    )
    return newUser.save().then((saved,error) => {
        if(error){
            console.log(error)
            return false
        } else {
            return true
        }
    })
}

/* 
{
    "firstName" : "John",
    "lastName" : "Doe",
    "age" : "35",
    "gender" : "Male",
    "email" : "john@mail.com",
    "password" : "john123",
    "mobileNo" : "109238123"
}
*/


// user login
/* 
    1. find if the email is existing in the database
    2. check if the password is correct

*/

module.exports.userLogin = (reqBody) => {
    return User.findOne ({email: reqBody.email}).then(result => {
        if (result === null) {
            return false
        } else {
            // compareSync function - used to compare a non encrypted password to an encrypted password and returns a boolean reasponse depending on gth result
            
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if (isPasswordCorrect) {
                return {access: auth.createAccessToken(result.toObject())}
            } else {
                return false
            }

            // auth - imported auth.js
            // createAccessToken - function inside the auth.js to create access token
        }
    })
}


module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        if (result === null) {
            return false
        } else {
            result.password = ""
            return result
        }
    })
}

/* 
mini activity
create getProfile function inside the userController
    1. find the id of the user using "data" as parameters
    2. if it does not exist, return false
    3. otherwires, return the details (stretch - make the password invisibl3e)
*/

/* 
    > find the user/document in the database
    >  add the courseId to the user's enrollment array
    > save the document in the database
*/
/* 
module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId)
    // adding the courseId to the users's enrollment array
    .then(user => {
        user.enrollments.push({courseId: data.courseId})
        return user.save().then((user,err) => {
            if(err) {
                return false
            } else {
                return true
            }
        })
    })
    if (isUserUpdated){
        return true 
    }
    else {
        return false
    }
}
 */

module.exports.enroll = async (data) => {
    // finding the user in the data base 
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollments.push( {courseId: data.courseId} )

		// saving in the database
		return user.save().then((user, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated){
		return true
	}
	else{
		return false
	}
}

/* 

{
    "courseId" : "626bea8512116db6e9276f04"
}
*/

/* 
    use another await keyword to update the enrollees array in the course collection 

    find the courseId from the requestBody
    push the userId of the enrollee in the enrollees array of the coures

    
*/




module.exports.enroll = async (data) => {
let isCourseUpdated = await Course.findById(data.courseId).then(course => {
    course.enrollees.push({userId: data.userId})
    return course.save().then((course, error) => {
        if(error){
            return false 
        } else {
            return true
        }
    })
})

if (isUserUpdated && isCourseUpdated) {
    return true 
}
else {
    return false
}

}
