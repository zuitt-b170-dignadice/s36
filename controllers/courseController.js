/* 
import the necessary models that you will need to perform CRUD operations
*/
const User = require("../models/user.js")
const Course = require("../models/course.js")

/* 

Activity s34
    1. Find the user in the database 
        find out if the user id admin
    
    2. If the user is not an admin, send the response "You are not an admin"

    3. If the user is an admin, create the new Course object
        - save the object 
            - if there are errors, return false
            - if there are no errors, return "course created successfully"

*/

module.exports.addCourse = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin ==  false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                courseName: reqBody.courseName,
                description : reqBody.description,
                price: reqBody.price
            })
            return newCourse.save().then((course,error) => {
                if(error){
                    return false
                } else {
                    return "course creation successful"
                }
            })
        
        }
    })
}


// retrieve all coutses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result
    })
}

module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
}


// retrieve all active courses
module.exports.getActiveCourses = () => {
    return Course.find({isActive: true}).then(result => {
        if (error) {
            console.log (error)    
        } else {
            return result
        }
    })
}
// update a course
module.exports.updateCourse = ( reqParams, reqBody ) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	// findByIdAndUpdate - looks for the id of the document (first parameter) and updates it (content of the second parameter)
		//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})

}
/* 
// update a course
module.exports.updateCourse = (reqParams, reqBody) =>  { 
    let updatedCourse = {
        courseName : reqBody.courseName,
        description : reqBody.description,
        price : reqBody.price

    }

    // looks for the id of the document (1st param) and updates it (content of the 2nd param)
        // the server will be lookin for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCOurse
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
        if (error) {
            return false
        } else {
            return true
        }
    })
} 
*/

// archive a course
/* 
 1. create an updateCourse object with nthe content from the reqBOdy

 reqBody should have the isAvtice status of the course to be set to false

2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate

    handle the errors that may arise
        error/s - false
        no errors - true
*/


module.exports.archivedCourse = (reqParams, reqBody ) => {
	let updatedCourse = {
		isActive: reqBody.isActive //  true or false will aslo do
    }
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})

}

