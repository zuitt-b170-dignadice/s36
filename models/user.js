const mongoose = require ("mongoose")

/* 
user schema

firstName - string
lastName - string
age - string
gender - string
email - string
password - string
mobileNo - stirng

isAdmin = boolean, false

enrollment array - will show the courses(id) where the students are enrolled and the data of their enrollemnt

push the updates on your s32 to 36 git repo with the commit message "add user schema" 
*/

const userSchema = new mongoose.Schema({
    firstName:{
        type: String,
        required : [true, "First Name is Required"]
    },
    lastName:{
        type: String,
        required : [true, "Last Name is Required"]
    },
    gender:{
        type: String,
        required : [true, "Gender is Required"]
    },
    email:{
        type: String,
        required : [true, "Email  is Required"]
    },
    password:{
        type: String,
        required : [true, "Password is Required"]
    },
    mobileNo:{
        type: String,
        required : [true, "Mobile number is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: true
    },
    enrollments: [
        {
            courseId: {
                type: String,
                required : [true, "User ID is required"]
            }
        },
        {
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ],
    status_user:{
        type: String,
        default: "Enrolled"
    }

})

module.exports = mongoose.model("User", userSchema)
