const mongoose = require ("mongoose")

const courseSchema = new mongoose.Schema({
    courseName:{
        type: String,
        required : [true, "Course Name is Required"]
    },
    description: { 
        type: String, 
        required: [true, "Course description is required"]
    },

    /* 
        create a similar data structure for price (number), isActive (boolean), created on (type: date, default value : new date)
    */

    price:{
        type: Number,
        required : [true, "Price for the course is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },

    /* 
        enrolees : array
             {userId: wqaerqwer12312344123, enrolled}
    */
    enrollees: [
        {
            userId: {
                type: String,
                required : [true, "User ID is required"]
            }
        },
        {
            enrolledOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
    
})

module.exports = mongoose.model("Course", courseSchema)